#include "example-student.h"

struct _ExampleStudent
{
  GObject parent_instance;

  gint roll_no;
};

G_DEFINE_TYPE (ExampleStudent, example_student, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ROLL_NO,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
example_student_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  ExampleStudent *self = EXAMPLE_STUDENT (object);

  switch (prop_id) {
    case PROP_ROLL_NO:
      g_value_set_int (value, example_student_get_roll_no (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
example_student_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  ExampleStudent *self = EXAMPLE_STUDENT (object);

  switch (prop_id) {
    case PROP_ROLL_NO:
      example_student_set_roll_no (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
example_student_class_init (ExampleStudentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = example_student_get_property;
  object_class->set_property = example_student_set_property;

  properties [PROP_ROLL_NO] =
    g_param_spec_int ("roll-no",
                      "Roll No",
                      "Whatever roll-no is",
                      G_MININT, G_MAXINT, 0,
                      G_PARAM_READWRITE |
                      G_PARAM_CONSTRUCT |
                      G_PARAM_STATIC_STRINGS |
                      G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
example_student_init (ExampleStudent *self)
{
}

ExampleStudent *
example_student_new (gint roll_no)
{
  return g_object_new (EXAMPLE_TYPE_STUDENT, "roll-no", roll_no, NULL);
}

gint
example_student_get_roll_no (ExampleStudent *self)
{
  g_return_val_if_fail (EXAMPLE_IS_STUDENT (self), 0);

  return self->roll_no;
}

void
example_student_set_roll_no (ExampleStudent *self,
                             gint            roll_no)
{
  g_return_if_fail (EXAMPLE_IS_STUDENT (self));

  if (self->roll_no == roll_no)
    return;

  self->roll_no = roll_no;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ROLL_NO]);
}
