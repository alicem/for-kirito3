#include "example-student.h"

gint
main (gint   argc,
      gchar *argv[])
{
  ExampleStudent *student, *student2;
  gint roll_no;

  student = example_student_new (10);

  // should be 10
  g_object_get (student, "roll-no", &roll_no, NULL);
  g_print ("%d\n", roll_no);

  example_student_set_roll_no (student, 20);

  // should be 20
  g_print ("%d\n", example_student_get_roll_no (student));

  g_object_set (student, "roll-no", 30, NULL);

  // should be 30
  g_print ("%d\n", example_student_get_roll_no (student));

  g_clear_object (&student);

  student2 = g_object_new (EXAMPLE_TYPE_STUDENT, "roll-no", 40, NULL);

  // should be 40
  g_print ("%d\n", example_student_get_roll_no (student2));

  g_clear_object (&student2);

  return 0;
}
